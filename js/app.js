(function(){
    const imgCount = 9;
    const thumbsOverlay = document.querySelector('.thumbs-overlay');
    const thumbsOverlayWidth = thumbsOverlay.clientWidth;
    const thumbsContainer = document.querySelector('.thumbs-container');
    let featuredImg = document.getElementById('featured-img');
    let moveMargin;

    //thumbnail set of 4
    const thumbNailSet = thumbsOverlayWidth / 4;
    thumbsContainer.style.width = thumbNailSet * imgCount + 'px';

    // calculate the max margin left thumbsContainer can go -- this is the thumbs slide
    let thumbsContainerMarginLeft = 0;
    let maxMoves = Math.ceil(imgCount /4) -1;
    let thumbsContainerMaxMargin = thumbsOverlayWidth * maxMoves;

      
    document.querySelectorAll('.top-ctls').forEach((item) => {
        item.addEventListener('click', (e) => {
            //console.log(e.target.parentNode.dataset.mainImg);
            let currImg = e.target.parentNode.parentNode.children[2];
            let currImgId = +currImg.dataset.mainImage;
            let newImgId;
         
            if(e.target.className == 'nextlg-btn'){
                if(currImgId == imgCount-1){
                    newImgId = currImgId;
                } else {
                    newImgId = currImgId +1;
            }
               
            } else {
                if(currImgId == 0){
                    newImgId = currImgId;
                } else {
                    newImgId = currImgId -1;
                }
            }

            let moveToImg = document.querySelector(`[data-thumb = "${newImgId}"]`);
            let moveToSrc = moveToImg.children[0].src;

            setFeaturedImg(newImgId, moveToSrc);
              
            //console.log(e.target.parentNode.parentNode.children[2]);
        })
    })





    // thumb name prev and next buttons
    document.querySelectorAll('.ctls').forEach((item) => {
        item.addEventListener('click', (e) => {
            if(e.target.className == 'next-btn'){
                if( thumbsContainerMarginLeft + 1 < thumbsContainerMaxMargin){
                    thumbsContainerMarginLeft =  thumbsContainerMarginLeft + thumbsOverlayWidth;
                }

            } else if(e.target.className == 'prev-btn'){
                if( thumbsContainerMarginLeft  > 0){
                    thumbsContainerMarginLeft  =  thumbsContainerMarginLeft - thumbsOverlayWidth;
                   }
            }

            thumbsContainer.style.marginLeft = `-${thumbsContainerMarginLeft}px`; 
        })
    })


    // thumbnail click event
    document.querySelectorAll('.thumb-img').forEach((item) => {
        item.addEventListener('click', (e) => {

            let clickedImg = e.target.src;
            let clickedParentId = e.target.parentElement.dataset.thumb;
            setFeaturedImg(clickedParentId, clickedImg);
         
        })
    })


    function setFeaturedImg(id, img){
        moveMargin = (Math.floor(id / 4)) * thumbsOverlayWidth;
        thumbsContainerMarginLeft = moveMargin;
        thumbsContainer.style.marginLeft = `-${moveMargin}px`; 
        featuredImg.dataset.mainImage = id;
        featuredImg.src = img;
    }
})()